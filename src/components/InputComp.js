import React from "react";
import { Button, Row, Col, Form } from "react-bootstrap";

export default class InputComp extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      num: 0,
      todo: "",
      isDone: false
    };
  }

  handleChange = () => {
    if (this.state.todo === "") {
      alert("Do somthing");
    } else {
      this.props.handleChange(this.state);
      this.setState({
        num: this.state.num + 1,
        todo: ""
      });
    }
  };

  handleChangeValue = event => {
    this.setState({
      todo: event.target.value
    });
  };

  render() {
    return (
      <Row className="justify-content-md-center" style={{margin:"20px"}}>
        <Col md={2}>
          <Form.Control
            type="text"
            placeholder="What to do?"
            value={this.state.todo}
            onChange={this.handleChangeValue}
          ></Form.Control>
        </Col>
        <Col md={1}>
          <Button
            variant="outline-success"
            type="submit"
            onClick={this.handleChange}
          >
            Add
          </Button>
        </Col>
      </Row>
    );
  }
}
