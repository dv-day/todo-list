import React from "react";
import { Table, Button } from "react-bootstrap";

export default class ListComp extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      container: {
        textDecoration: ""
      },
      variant: "success",
      shouldHide: true
    };
  }

  addTable = props => {
    return this.props.tableData.map((item, i) => (
      <tr>
        <td key={i}>{i + 1}</td>
        <td style={{ textDecoration: item.isDone ?"line-through" :"" }}>{item.todo}</td>
        <td>
          {item.isDone ? (
            <Button
              variant="danger"
              onClick={() => {
                this.props.removeRow(i);
              }}
            >
              Remove
            </Button>
          ) : (
            <Button
              variant={this.state.variant}
              onClick={() => {
                this.props.onClickDoneBtn(i);
              }}
            >
              Done
            </Button>
          )}
        </td>
      </tr>
    ));
  };

  render() {
    return (
      <div>
        <Table
          style={{ width: "80%", margin: "20px 10%", tableLayout: "fixed" }}
          striped
          bordered
          hover
          variant="outline"
          responsive
          size="md"
        >
          <thead>
            <tr>
              <th>#</th>
              <th>ToDo</th>
              <th></th>
            </tr>
          </thead>
          <tbody>{this.addTable()}</tbody>
        </Table>
      </div>
    );
  }
}
