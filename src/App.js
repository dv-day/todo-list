import React from "react";
import "./App.css";
import InputComp from "./components/InputComp";
import ListComp from "./components/ListComp";

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      lists: [],
    };
  }

  listToArr = (list) => {
    const arr = [...this.state.lists, list];
    this.setState({
      lists: arr,
    });
    console.log(arr);
  };

  removeFromArr = (ind) => {
    alert("Are you sure ?");
    const arr2 = [...this.state.lists];
    if (ind !== -1) {
      arr2.splice(ind, 1);
      this.setState({
        lists: arr2,
      });
    }
    console.log(this.state.lists);
  };

  setDone = (ind) => {
    alert("Nice, carry on!");
    const arr3 = [...this.state.lists];
    arr3[ind].isDone = true;
    this.setState({
      lists: arr3,
    });
  };

  render() {
    return (
      <div>
        <InputComp handleChange={this.listToArr}></InputComp>
        <ListComp
          tableData={this.state.lists}
          removeRow={this.removeFromArr}
          onClickDoneBtn={this.setDone}
        ></ListComp>
      </div>
    );
  }
}

export default App;
